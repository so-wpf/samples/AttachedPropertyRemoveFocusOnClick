﻿
namespace AttachedPropertyRemoveFocusOnClick
{
    using System.Windows;
    using System.Windows.Controls.Primitives;
    using System.Windows.Input;

    public static class Focus
    {
        public static readonly DependencyProperty RemoveOnClickProperty = DependencyProperty.RegisterAttached(
            "RemoveOnClick",
            typeof(bool),
            typeof(Focus),
            new PropertyMetadata(default(bool)));

        static Focus()
        {
            EventManager.RegisterClassHandler(typeof(ButtonBase), ButtonBase.ClickEvent, new RoutedEventHandler(OnCLick));
        }

        public static void SetRemoveOnClick(ButtonBase element, bool value)
        {
            element.SetValue(RemoveOnClickProperty, value);
        }

        public static bool GetRemoveOnClick(ButtonBase element)
        {
            return (bool)element.GetValue(RemoveOnClickProperty);
        }

        private static void OnCLick(object sender, RoutedEventArgs e)
        {
            if (sender is ButtonBase button &&
                GetRemoveOnClick(button) &&
                button.IsFocused)
            {
                var scope = FocusManager.GetFocusScope(button);
                if (scope != null)
                {
                    FocusManager.SetFocusedElement(scope, null); // remove logical focus
                }

                if (button.IsKeyboardFocused)
                {
                    Keyboard.ClearFocus(); // remove keyboard focus
                }
            }
        }
    }
}
